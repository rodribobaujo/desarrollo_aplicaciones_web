**Desarrollo de aplicaciones web con conexión a base de datos**
**Alumnos:**
*Rodrigo Bobadilla Araujo*

- Practica #1 - 02/09/2022 - Practica de ejemplo 
Commit: 45fe4d2e5cea07a5aca099cd9821d93338e4d689
Archivo: https://gitlab.com/rodribobaujo/desarrollo_aplicaciones_web/-/blob/main/parcial_1/practica_ejemplo.html

- Practica #2 - 09/09/2022 - Mensajes en JavaScript
Commit: 47f1745e87d07865f98862bf924a365de7dd4555
Archivo: https://gitlab.com/rodribobaujo/desarrollo_aplicaciones_web/-/blob/main/parcial_1/practicaJavaScript.html

- Práctica #3 - 15/09/2022 - Práctica web con bases de datos - parte 1
Commit: 101a31f7b051b6d80fe5d2eef69c8c3704877f71

- Práctica #4 - 19/09/2022 - Práctica web con bases de datos - Vista de consulta de datos
Commit: 7b5621699bc8948d32b1a9f869af9cc2a547f07c

- Práctica #5 - 22/09/2022 - Práctica web con bases de datos - Vista de registro de datos
Commit: 96010d2010a5e0db0216c642fb19cb0174ab5cf9

- Práctica #6 - 26/09/2022 - Práctica web con bases de datos - Conexión de base de datos
Commit: b7ce2a60b445f57dd075f22889a900c513e6074c
